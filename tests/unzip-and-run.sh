set -o errexit

TMPDIR=`mktemp --directory`

echo "temporary directory for tests:" $TMPDIR

unzip -qq public/tp-crypto-rsa-*.zip -d $TMPDIR

cp tests/post-build-tests.py $TMPDIR/ma_solution.py

cd $TMPDIR
bash installer.sh
bash lancer.sh