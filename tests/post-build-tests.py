import os

from lib_tp_rsa import recuperer_certificat
recuperer_certificat('python.org')

print("post-build tests OK")

# required as using exit() or sys.exit() would not stop the program
# if invoked via "python -i"
os._exit(0)