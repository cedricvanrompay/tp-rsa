const REGEXP_A_REMPLACER = new RegExp(/(\s*#[ ]*)(\^-+\^) À REMPLACER/);
// flag "g" is required for use in replaceAll
const REGEXP_BLOCK_PLACEHOLDER = new RegExp(/^# BLOCK: (.*)$/, "mg");

export function loadBlocks(source) {
    const blocks = new Map();
    let currentBlock = null;

    let lineNb = 1;
    for (const line of source.split("\n")) {
        if (line.startsWith("# BLOCK:")) {
            if (currentBlock != null) {
                throw new Error(`line ${lineNb}: beginning of block while already in a block: ${line}`)
            }

            currentBlock = {
                name: line.slice("# BLOCK: ".length),
                lines: [],
                latestLine: null,
            }
        } else if (line.startsWith("# ENDBLOCK")) {
            if (currentBlock == null) {
                throw new Error(`line ${lineNb}: end of block while there is no current block: ${line}`);
            }

            if (currentBlock.latestLine != null) {
                currentBlock.lines.push(currentBlock.latestLine);
            }
            currentBlock.latestLine = null;

            // XXX ideally we would try to avoid adding empty lines at the start
            // instead of using trimStart
            blocks.set(currentBlock.name, currentBlock.lines.join("\n").trimStart());

            currentBlock = null;
        } else if (currentBlock != null) {
            const match = line.match(REGEXP_A_REMPLACER);

            if (match) {
                const start = match[1].length;
                const end = start + match[2].length;

                currentBlock.latestLine = (
                    currentBlock.latestLine.slice(0, start)
                    + "#[À REMPLACER]#"
                    + currentBlock.latestLine.slice(end)
                )
            } else {
                if (currentBlock.latestLine != null) {
                    currentBlock.lines.push(currentBlock.latestLine);
                }
                currentBlock.latestLine = line;
            }

        }

        lineNb++;
    }

    return blocks
}

/**
 * 
 * @param {string} markdown 
 * @param {Map<string, string>} blocks 
 * @returns {string}
 */
export function insertCodeBlocks(markdown, blocks) {
    return markdown.replaceAll(
        REGEXP_BLOCK_PLACEHOLDER,
        (_, blockName) => {
            if (!blocks.has(blockName)) {
                throw new Error(`block name not found: ${blockName}`);
            }

            return blocks.get(blockName);
        }
    )
}