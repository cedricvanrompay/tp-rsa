import { readFileSync, writeFileSync, mkdirSync } from "node:fs";

import { Marked } from 'marked';
import { markedHighlight } from "marked-highlight";
import hljs from 'highlight.js';

export function renderHtml(markdown) {
    const marked = new Marked(
        markedHighlight({
            langPrefix: 'hljs language-',
            highlight(code) {
                return hljs.highlight(code, { language: "python" }).value;
            }
        })
    )
    
    const notesRendered = markdown.replaceAll(
        /<div class="note">([^<]+)<\/div>/g,
        (_, note) => `<div class="note">${marked.parseInline(note)}</div>`,
    )
    const body = marked.parse(notesRendered);

    const template = readFileSync("template.html", { encoding: "utf-8" });

    const html = template.replace("</body>", `${body}\n</body>`)

    return html;
}

