import katex from 'katex';

// TODO consider using https://github.com/UziTech/marked-katex-extension

const REGEXP_DISPLAY_MATH = new RegExp(
    /\$\$([^\$]+)\$\$/,
    // g: global, required for replaceAll
    // s: "dot all", dot can match newlines
    "gs",
)

const REGEXP_INLINE_MATH = new RegExp(
    /\$([^\$]+)\$/,
    // g: global, required for replaceAll
    "g",
)

export function renderMath(markdown) {
    return markdown
        .replaceAll(
            REGEXP_DISPLAY_MATH,
            (_, latex) => katex.renderToString(latex, { displayMode: true }),
        )
        .replaceAll(
            REGEXP_INLINE_MATH,
            (_, latex) => {
                try {
                    return katex.renderToString(latex, { displayMode: false })
                } catch (error) {
                    error.message = `processing inline math "${latex}": ` + error.message;
                    throw error;
                }
            },
        );
}