import { readFileSync, writeFileSync, mkdirSync } from "node:fs";

import { insertCodeBlocks, loadBlocks } from "./insert-code-blocks.js";
import { renderHtml } from "./render-html.js";
import { renderMath } from "./render-math.js";

const blocks = loadBlocks(readFileSync("solutions.py", { encoding: "utf-8" }));

const source = readFileSync("instructions.md", { encoding: "utf-8" });

const html = renderHtml(renderMath(insertCodeBlocks(source, blocks)));

try {
    mkdirSync("public");
} catch (error) {
    if (error.code != "EEXIST") {
        throw error;
    }
}
writeFileSync("public/instructions.html", html);