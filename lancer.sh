set -o errexit

if [ ! -d .venv ]; then
    (>&2 echo -e "ERREUR: installation manquante ?\nEssayer \"bash installer.sh\"" && exit 1)
fi

.venv/bin/python -i ma_solution.py