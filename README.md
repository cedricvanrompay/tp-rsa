# Sujet de Travaux Pratiques sur la Signature RSA conçu pour des Lycéens

https://cedricvanrompay.gitlab.io/tp-rsa/

Il s'agit bien de *signature* RSA et non de *chiffrement*.
RSA peut être utilisé à la fois pour le chiffrement et pour la signature,
et s'il est le plus souvent présenté aux novices comme un mécanisme de chiffrement,
en pratique il est plus souvent utilisé pour la signature.
