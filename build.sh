set -o errexit

rm -rf public
mkdir public

cd ./render-intructions/
npm ci
cd ..
node ./render-intructions/

ARCHIVE_NAME="tp-crypto-rsa-$(date +%FT%H-%M-%SZ --utc).zip"
rm -rf to-zip
mkdir to-zip
cp static/*.png static/main.css to-zip
cp public/instructions.html to-zip
cp lib_tp_rsa.py cles_publiques_alice_et_bob.json conversation.json installer.sh lancer.sh to-zip
echo "# mettez ici votre solution au TP" > to-zip/ma_solution.py
cp -r .tools to-zip
# we need a virtual environment for pipenv so that we don't break the system python;
# and then we'll need another virtual environment to execute the user code
python -m venv .pipenv-venv
.pipenv-venv/bin/python -m pip install pipenv
.pipenv-venv/bin/python -m pipenv requirements > to-zip/.tools/requirements.txt
cd to-zip
zip -r "../public/$ARCHIVE_NAME" .
cd ..
rm -rf .pipenv-venv

sed "s/TODO REPLACE WITH ZIP FILE NAME/$ARCHIVE_NAME/" index.html > public/index.html

python -m venv .venv
.venv/bin/python -m pip install -r to-zip/.tools/requirements.txt
.venv/bin/python test-website-cert-validation.py
.venv/bin/python tests.py
rm -rf .venv

bash tests/unzip-and-run.sh

cp static/* public
