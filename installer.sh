set -o errexit

which python > /dev/null || (>&2 echo "ERREUR: impossible de trouver python" && exit 1)

python .tools/setup.py