import sys

from lib_tp_rsa import recuperer_certificat
from solutions import *

successes = list()
failures = list()

for website in [
    "python.org", "google.com",
    "ubuntu.com", "cedricvanrompay.fr"
]:
    try:
        cert_site, cert_autorite = recuperer_certificat(website)

        if verifier_certificat_site(cert_site, cert_autorite):
            successes.append(website)
        else:
            failures.append(website)
    except Exception as err:
        failures.append(website)
        print(f"ERROR: testing website {website}: {err}")
        continue

if successes:
    print("success for:", successes)
if failures:
    print("failure for:", failures)

if failures:
    sys.exit(1)