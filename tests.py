'teste le bon fonctionnement des solutions'

from random import randint

from solutions import *

assert signature_rsa_est_valide(42, 70, 3, 187)
assert not signature_rsa_est_valide(45, 70, 3, 187)

D = 107
N = 187
E = 3
M = 57
faux_M = 64
S = signer_rsa(M, D, N)
assert signature_rsa_est_valide(M, S, E, N)
assert not signature_rsa_est_valide(faux_M, S, E, N)

(E, D, N) = generer_cle_rsa()
M = 135
faux_M = 28
S = signer_rsa(M, D, N)
assert signature_rsa_est_valide(M, S, E, N)
assert not signature_rsa_est_valide(faux_M, S, E, N)

(E, D, N) = generer_cle_rsa()
msg = "Bien sûr que oui !"
faux_msg = "Bien sûr que non !"
S = signer_message_rsa(msg, D, N)
assert signature_message_rsa_est_valide(msg, S, E, N)
assert not signature_message_rsa_est_valide(faux_msg, S, E, N)


from lib_tp_rsa import charger_conversation_depuis_json, charger_cles_depuis_json

liste_messages = charger_conversation_depuis_json('conversation.json')
cles_publiques = charger_cles_depuis_json('cles_publiques_alice_et_bob.json')

auth_conv = conversation_authentifiee(liste_messages, cles_publiques)
assert (auth_conv ==
"""\
# 10h31 Alice (signature OK):
Salut Bob, ça va ? Dis tu me dois toujours 20€ pour le resto.

# 10h34 Bob (signature OK):
Salut. Ah oui c'est vrai. Je peux te faire un virement ?

# 10h35 Alice (signature OK):
OK. Attend je t'envoies mon RIB (Relevé d'Identité Bancaire)

# 10h36 Alice (signature NON VALIDE !!!):
RIB: 27-48715236-84

# 10h39 Bob (signature OK):
Ça y est, c'est envoyé :-)

# 10h52 Alice (signature OK):
T'es sûr ? J'ai pas reçu l'argent.

# 10h55 Bob (signature OK):
Quoi ? Attends moi j'ai été prévelé sur mon compte !

""")

from lib_tp_rsa import recuperer_certificat

cert_site, cert_autorite = recuperer_certificat("python.org")
assert verifier_certificat_site(cert_site, cert_autorite)
