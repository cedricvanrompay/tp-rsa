set -o errexit

apt-get update
# for nodesource
apt-get install -y ca-certificates curl gnupg
# from https://github.com/nodesource/distributions/blob/4f746d991e099f9494b12e275abe1aab21f1ddf0/README.md
mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
NODE_MAJOR=20
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
apt-get update
apt-get install nodejs -y

apt-get install -y python3 python3-venv python3-pip python-is-python3 zip

# from https://docs.docker.com/develop/develop-images/instructions/#apt-get
rm -rf /var/lib/apt/lists/*