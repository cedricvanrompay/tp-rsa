# BLOCK: signature et vérification

def signer_rsa(M, D, N):
    S = pow(M, D, N)
    #   ^----------^ À REMPLACER
    return S

def signature_rsa_est_valide(M, S, E, N):
    '''renvoie « True » si S est une signature du message M
    faite avec la clé privée correspondant à (E, N);
    sinon, renvoie « False »'''
    M_2 = pow(S, E, N)
    #     ^----------^ À REMPLACER
    if M_2 == M:
        return True
        #      ^--^ À REMPLACER
    else:
        return False
        #      ^---^ À REMPLACER

# ENDBLOCK signature et vérification


# BLOCK: génération de clé

# voici des fonctions fournies par le fichier lib_tp_rsa.py
# que l'on importe pour les utiliser dans notre fonction "generer_cle_rsa"
# voir leur documentation dans le fichier lib_tp_rsa.py
from lib_tp_rsa import generer_premier, sont_premiers_entre_eux, inverse_modulo

def generer_cle_rsa():
    "génère une paire de clés RSA"
    E = 3
    
    nombre_max_tentatives = 5
    for i in range(nombre_max_tentatives):
        P = generer_premier()
        #   ^---------------^ À REMPLACER
        Q = generer_premier()
        #   ^---------------^ À REMPLACER

        N = P*Q
        #   ^-^ À REMPLACER
        phi_N = (P-1)*(Q-1)
        #       ^---------^ À REMPLACER

        if ( P != Q and sont_premiers_entre_eux(E, phi_N) ):
        #               ^-------------------------------^ À REMPLACER
            D = inverse_modulo(E, phi_N)
            #   ^----------------------^ À REMPLACER

            # "return" va nous faire sortir de la fonction
            # et donc de la boucle "for" aussi
            return (E, D, N)

    # si on arrive là c'est que toutes les tentatives ont échoué
    # (on n'a jamais atteint le "return")
    # donc on renvoie une Erreur avec "raise Exception()"
    raise Exception('toutes les tentatives ont échoué')

# ENDBLOCK: génération de clé


# BLOCK: signer de vrais messages

from lib_tp_rsa import encoder_msg_en_nombre

def signer_message_rsa(msg, D, N):
    M = encoder_msg_en_nombre(msg, N)
    S = signer_rsa(M, D, N)
    #   ^-----------------^ À REMPLACER
    return S

# ENDBLOCK: signer de vrais messages


# BLOCK: vérifier de vrais messages

def signature_message_rsa_est_valide(msg, S, E, N):
    M = encoder_msg_en_nombre(msg, N)
    #   ^---------------------------^ À REMPLACER
    if signature_rsa_est_valide(M, S, E, N):
    #  ^----------------------------------^ À REMPLACER
        return True
    else:
        return False

# ENDBLOCK: vérifier de vrais messages


# BLOCK: conversation authentifiée

def conversation_authentifiee(liste_messages, cles_publiques):
    # cette variable stockera le texte a afficher
    resultat = str()

    for message in liste_messages:
        heure = message["heure"]
        exp = message["expéditeur"]
        msg = message["texte"]
        S = message["signature"]

        # réfléchissez: quelle variable contient le nom de l'expéditeur
        # dont on a besoin pour récupérer la clé publique ?
        E, N = cles_publiques[ exp ]
        #                      ^-^ À REMPLACER

        if signature_message_rsa_est_valide(msg, S, E, N) :
        #  ^--------------------------------------------^ À REMPLACER
            sig_valide = "OK"
        else:
            sig_valide = "NON VALIDE !!!"

        # On ajoute le message au résultat

        resultat += f'# {heure} {exp} (signature {sig_valide}):'
        # ajoute un retour à la ligne
        resultat += '\n'
        resultat += msg
        resultat += '\n\n'

    # on fait un "return" du résultat plutôt qu'un "print",
    # c'est une bonne habitude à prendre quand on écrit des fonctions
    return resultat

# ENDBLOCK: conversation authentifiée


# BLOCK: vérification de certificats

def verifier_certificat_site(cert_site, cert_autorite):
    # la version "à signer" du certificat (notre "message")
    # ("TBS" est pour "To Be Signed", "À signer" en anglais)
    msg = cert_site.tbs_certificate_bytes
    
    # La signature du certificat du site
    S = cert_site.signature
    S = int.from_bytes(S, byteorder='big')
    
    # La clé publique de l'autorité que l'on va stocker dans deux variables N et E
    N = cert_autorite.public_key().public_numbers().n
    E = cert_autorite.public_key().public_numbers().e
    
    # À vous de jouer !
    # vous avez déjà codé la fonction qu'il faut utiliser ici,
    # il n'y a qu'à l'appeler avec les bons paramètres !
    
    if signature_message_rsa_est_valide(msg, S, E, N) :
    #  ^--------------------------------------------^ À REMPLACER
        return True
    else:
        return False

# ENDBLOCK: vérification de certificats

# BLOCK: factorisation

from math import sqrt

def factoriser(N):
    for i in range(2, int(sqrt(N))):
    #                     ^-----^ À REMPLACER
        if N%i == 0:
        #  ^------^ À REMPLACER
            P = i
            Q = N//i
            #   ^--^ À REMPLACER
            return P, Q

# ENDBLOCK: factorisation
