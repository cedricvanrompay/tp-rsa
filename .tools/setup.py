import os
import subprocess
import sys
import venv

PATH_TO_VENV = ".venv"
PATH_TO_REQUIREMENTS = ".tools/requirements.txt"

if os.path.exists(PATH_TO_VENV):
    sys.exit(f"ERREUR: le dossier \"{PATH_TO_VENV}\" existe déjà")

if not os.path.isfile(PATH_TO_REQUIREMENTS):
    sys.exit(f"ERREUR: fichier \"{PATH_TO_REQUIREMENTS}\" introuvable")

print("installation...")

venv.EnvBuilder(
    symlinks=True,
    with_pip=True,
    prompt="tp-rsa-venv",
).create(PATH_TO_VENV)

subprocess.run(
    f"{PATH_TO_VENV}/bin/python -m pip install -r {PATH_TO_REQUIREMENTS}".split(),
    check=True,
    capture_output=True,
)